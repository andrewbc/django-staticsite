#!/usr/bin/env bash
kill -s HUP `supervisorctl status | sed -n '/RUNNING/s/.*pid \([[:digit:]]\+\).*/\1/p'`